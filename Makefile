.PHONY: app cm secret
app:
	kubectl apply -f application.yml -n argocd
cm:	
	kubectl apply -f argocd-notifications-cm.yml -n argocd
secret:
	kubectl apply -f argocd-notifications-secret.yml -n argocd

